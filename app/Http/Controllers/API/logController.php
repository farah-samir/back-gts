<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use Auth;

use App\Http\Controllers\API\BaseController as BaseController;

use  Validator ;



class logController extends Controller
{
  public function index(){

$log = log::get();
echo json_encode($log);

  }
  public function show($log_id){
    return Log::find($log_id);
  }



public function store(Request $request){
    $log = new Log();
    $log->name = $request->input('name');
    $log->email = $request->input('email');
    $log->password = $request->input('password');
    $log->c_password = $request->input('c_password');
    $log->save();
    echo json_encode($log);

}

public function update(Request $request,  $log_id){
    $log =  Log::find($log_id);
    $log->email = $request->input('email');
    $log->password = $request->input('password');
    $log->save();
    echo json_encode($log);

}

               public function destroy($log_id){
               $log = log::find($log_id);
                      $log->delete();

}

public function register(Request $request)
{
    # code...

    $validator =    Validator::make($request->all(), [
    'name'=> 'required',
    'email'=> 'required|email',
    'password'=> 'required',
     'c_password' => 'required|same:password',
    ] );

    if ($validator -> fails()) {
        # code...
        return $this->sendError('error validation', $validator->errors());
    }

    $input = $request->all();
    $input['password'] = bcrypt($input['password']);
    $user = User::create($input);
    $success['token'] = $user->createToken('MyApp')->accessToken;
    $success['email'] = $user->email;

    return $this->sendResponse($success , 'User created succesfully');

}


}
