<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\home;

class HomeController extends Controller
{

  public function index(){

$home = home::orderBy('created_at', 'desc')->get();
echo json_encode($home);

  }
  public function show($home_id){
    return Home::find($home_id);
  }

public function store(Request $request){
    $home = new Home();
      $home->cin = $request->input('cin');
      $home->nom = $request->input('nom');
      $home->prenom = $request->input('prenom');
      $home->adresse = $request->input('adresse');
      $home->date = $request->input('date');
      $home->telephone = $request->input('telephone');
      $home->situations = $request->input('situations');
      $home->civilite = $request->input('civilite');
      $home->email = $request->input('email');
      $home->code = $request->input('code');
       $home->departement = $request->input('departement');
        $home->duration = $request->input('duration');
    $home->lieu = $request->input('lieu');
      $home->mois = $request->input('mois');
        $home->sl = $request->input('sl');
          $home->dated = $request->input('dated');
            $home->datef = $request->input('datef');
              $home->semaine = $request->input('semaine');
                $home->typ = $request->input('typ');
                  $home->horaire= $request->input('horaire');
                    $home->poste = $request->input('poste');
                    $home->paie = $request->input('paie');
                      $home->numero= $request->input('numero');
                        $home->prime = $request->input('prime');

      $home->save();
    echo json_encode($home);

}

public function update(Request $request,  $home_id){
    $home =  Home::find($home_id);

    $home->cin = $request->input('cin');
    $home->nom = $request->input('nom');
    $home->prenom = $request->input('prenom');
    $home->adresse = $request->input('adresse');
    $home->date = $request->input('date');
    $home->telephone = $request->input('telephone');
    $home->situations = $request->input('situations');
    $home->civilite = $request->input('civilite');
    $home->email = $request->input('email');
    $home->code = $request->input('code');
     $home->departement = $request->input('departement');
    $home->duration = $request->input('duration');
  $home->lieu = $request->input('lieu');
    $home->mois = $request->input('mois');
      $home->sl = $request->input('sl');
        $home->dated = $request->input('dated');
          $home->datef = $request->input('datef');
            $home->semaine = $request->input('semaine');
                  $home->typ = $request->input('typ');
          $home->horaire= $request->input('horaire');
      $home->poste = $request->input('poste');
        $home->paie = $request->input('paie');
          $home->numero= $request->input('numero');
  $home->prime = $request->input('prime');

    $home->save();
  echo json_encode($home);

}

               public function destroy($home_id){
               $home = Home::find($home_id);
                      $home->delete();

}

}
