<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Response;

use Illuminate\Http\Request;
use App\Movies;

class MoviesController extends Controller
{
  public function index(){

$movies = Movies::get();
echo json_encode($movies);

  }
  public function show($movies_id){
    return Movies::find($movies_id);
  }



public function store(Request $request){
    $movies = new Movies();
    $movies->name = $request->input('name');
    $movies->description = $request->input('description');
    $movies->genre = $request->input('genre');
    $movies->year = $request->input('year');
    $movies->duration = $request->input('duration');

      $movies->save();
    echo json_encode($movies);

}

public function update(Request $request,  $movies_id){
    $movies =  Movies::find($movies_id);

    $movies->name = $request->input('name');
    $movies->description = $request->input('description');
    $movies->genre = $request->input('genre');
    $movies->year = $request->input('year');
    $movies->duration = $request->input('duration');

      $movies->save();
    echo json_encode($movies);

}

               public function destroy($movies_id){
               $movies = Movies::find($movies_id);
                      $movies->delete();

}




}
