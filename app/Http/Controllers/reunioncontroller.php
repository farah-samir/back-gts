<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\reunion;
use App\home;

class reunioncontroller extends Controller
{
  public function index(){
     $reunion = Reunion::orderBy('created_at', 'desc')->with('home')->get();
     echo json_encode($reunion);

  }


  public function show($reunion_id){
    return Reunion::find($reunion_id);
  }



public function store(Request $request){
    $reunion = new Reunion();
    $reunion->home_id = $request->input('home_id');
    $reunion->sujet = $request->input('sujet');
    $reunion->horaire = $request->input('horaire');
    $reunion->date = $request->input('date');
 

      $reunion->save();
    echo json_encode($reunion);

}

public function update(Request $request,  $reunion_id){
    $reunion =  Reunion::find($reunion_id);
    $reunion->home_id = $request->input('home_id');
    $reunion->sujet = $request->input('sujet');
    $reunion->horaire = $request->input('horaire');
    $reunion->date = $request->input('date');


      $reunion->save();
    echo json_encode($reunion);

}

               public function destroy($reunion_id){
               $reunion = Reunion::find($reunion_id);
                      $reunion->delete();

}

}
