<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class home extends Model
{
    protected $fillable = [

      'cin','nom','prenom','adresse','date','telephone','situations','civilite','email','code','departement',
          'duration','lieu','mois','sl','dated','datef','semaine','typ','horaire','poste','paie','numero','prime'
    ];

    public function reunions(){
        return $this->hasMany('app\reunion');
      }

   
      public function paies(){
        return $this->hasMany('app\paie');
      }

         
      public function ordres(){
        return $this->hasMany('app\ordre');
      }

}
