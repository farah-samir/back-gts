<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCongesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conges', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cin');
            $table->string('prenom');
            $table->string('nom');
            $table->string('genreconge');
            $table->string('congeformation');
            $table->string('congecitoyen');
            $table->string('dates');
            $table->string('number1');
            $table->string('datee');
            $table->string('number2');
            $table->string('resultats');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conges');
    }
}
