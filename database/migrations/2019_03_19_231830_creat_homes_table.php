<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatHomesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('homes', function (Blueprint $table) {
          $table->increments('id');
          $table->string('cin');
           $table->string('nom');
           $table->string('prenom');
           $table->string('adresse');
           $table->string('date');
           $table->string('telephone');
           $table->string('situations');
           $table->string('civilite');
           $table->string('email');
           $table->string('code');
           $table->string('departement');
           $table->string('duration');
           $table->string('lieu');
           $table->string('mois');
           $table->string('sl');
           $table->string('dated');
           $table->string('datef');
           $table->string('semaine');
           $table->string('typ');
           $table->string('horaire');
           $table->string('poste');
           $table->string('paie');
           $table->string('numero');
              $table->string('prime');
          $table->timestamps();



      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::dropIfExists('homes');
    }
}
