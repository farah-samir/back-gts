<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::resource('/movies','MoviesController');
Route::resource('task','TaskController');
//Route::resource('/login','LoginController');
Route::resource('/recrute','recruteController');
Route::resource('/log','logController');
Route::resource('/home','homeController');
Route::resource('/conge','CongeController');
Route::resource('/reunion','reunioncontroller');
Route::resource('/paie','PaieController');
Route::resource('/ordre','OrderController');
Route::resource('/editor','EditorController');
Route::resource('/calendre','CalendreController');
//Route::post('login', 'API\UserController@login');
route::resource('/biodata','BiodataController');
//Route::resource('/user','API\UserController');

//Route::post('/user', 'API\UserController@register');
//Route::post('/login', 'API\UserController@login');
Auth::routes();


Route::group([
    'middleware' => 'api',
], function () {

    Route::post('login', 'AuthController@login');
    Route::get('getuser', 'AuthController@getuser');

    Route::post('signup', 'AuthController@signup');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::post('sendPasswordResetLink', 'ResetPasswordController@sendEmail');
    Route::post('resetPassword', 'ChangePasswordController@process');
});
